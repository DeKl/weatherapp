var map, infoWindow, currentPositionLat, currentPositionLon;

openRefusalWindowBackground = () => {
  document.getElementById(
    "modalWindowForPositionRefusalBackground"
  ).style.display = "initial";
};

openModalWindow = () => {
  document.getElementById(
    "modalWindowForPositionRefusal"
  ).innerHTML = `このサービスを使用するには、現在の位置の共有が必要です。ポジションへのアクセスを有効にして、ページをリロードしてください
    `;
  document.getElementById("modalWindowForPositionRefusal").style.display =
    "initial";
};

makeWindowUnaccessable = () => {
  openRefusalWindowBackground();
  openModalWindow();
};
const getCurrentLocationCoordinates = () => {
  return new Promise(function(resolve, reject) {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function getcurrentCoordinates(
        position
      ) {
        currentPositionLat = position.coords.latitude;
        currentPositionLon = position.coords.longitude;
        combinedLocationCoordinates = {
          currentPositionLat,
          currentPositionLon
        };
        resolve(combinedLocationCoordinates);
      },
      reject);
    } else {
      reject("couldnt get current user location coordiantes");
    }
  });
};

const initAndSetMap = combinedLocationCoordinates => {
  latCoordinate = combinedLocationCoordinates.currentPositionLat;
  lngCoordinate = combinedLocationCoordinates.currentPositionLon;
  map = new google.maps.Map(document.getElementById("googleMap"), {
    center: { lat: latCoordinate, lng: lngCoordinate },
    zoom: 8
  });
  var pos = {
    lat: latCoordinate,
    lng: lngCoordinate
  };
  infoWindow = new google.maps.InfoWindow();
  infoWindow.setPosition(pos);
  infoWindow.setContent("現在地");
  infoWindow.open(map);
  map.setCenter(pos);
};

const getCurrentWeatherInformation = (
  currentPositionLat,
  currentPositionLon
) => {
  return fetch(
    `https://darksky-proxy.theshark.now.sh/weather/${currentPositionLat}/${currentPositionLon}`
  ).then(response => response.json());
};

const defineWeatherDataAssignmentForUpdatingOfUserInterface = receivedWeatherData => {
  let weatherAndLocDataObject = {
    geoInformationNowCountry: receivedWeatherData.timezone,
    geoInformationNowLat: receivedWeatherData.latitude,
    geoInformationNowLon: receivedWeatherData.longitude,
    mainWeathNow: receivedWeatherData.currently.summary,
    feltWeatherDescNow: (
      (receivedWeatherData.currently.apparentTemperature - 32) *
      (5 / 9)
    ).toFixed(1),
    tempNow: (
      (receivedWeatherData.currently.temperature - 32) *
      (5 / 9)
    ).toFixed(1),
    windSpeedNow: receivedWeatherData.currently.windSpeed,
    humidNow: receivedWeatherData.currently.humidity * 100,
    foreCastNow: receivedWeatherData.hourly.summary
  };

  updateTheUserInterface(weatherAndLocDataObject);
};

const updateTheUserInterface = assignedDataValueObject => {
  const elementTemplate = {};
  document.querySelectorAll("[data-mapping]").forEach(element => {
    const dataMappingValueName = element.dataset.mapping;
    if (!elementTemplate[dataMappingValueName]) {
      elementTemplate[dataMappingValueName] = [];
    }
    elementTemplate[dataMappingValueName].push(element);
  });

  Object.entries(assignedDataValueObject).forEach(
    ([dataMappingValueNameForComparison, numberValue]) => {
      (elementTemplate[dataMappingValueNameForComparison] || []).forEach(el => {
        el.innerHTML = numberValue;
      });
    }
  );
};

const triggerAsync = async () => {
  try {
    await getCurrentLocationCoordinates().then(initAndSetMap);
  } catch (e) {
    makeWindowUnaccessable();
  }
  getCurrentWeatherInformation(currentPositionLat, currentPositionLon).then(
    defineWeatherDataAssignmentForUpdatingOfUserInterface
  );
};

window.triggerAsync = triggerAsync;

/* selectMenuintegrieren*/
/*On change eventlistener für die Select Optionen, welche die Schleife aufruft mit den Daten*/

cities = {
  Berlin: {
    currentPositionLat: 52.520008,
    currentPositionLon: 13.404954
  },
  Amsterdam: {
    currentPositionLat: 52.379189,
    currentPositionLon: 4.899431
  },
  Tokyo: {
    currentPositionLat: 35.652832,
    currentPositionLon: 139.839478
  },
  Shenzhen: {
    currentPositionLat: 22.54883,
    currentPositionLon: 114.062996
  },
  SanFransisco: {
    currentPositionLat: 37.773973,
    currentPositionLon: -122.431297
  }
};

document
  .getElementById("locationOptionSelect")
  .addEventListener("change", function() {
    x = this.value;
    filterAndDisplayCityData(x);
  });

filterAndDisplayCityData = cityName => {
  if (cityName == 0) {
  } else {
    filteredCity = cities[cityName];
    initAndSetMap(filteredCity);
    getCurrentWeatherInformation(
      filteredCity.currentPositionLat,
      filteredCity.currentPositionLon
    ).then(defineWeatherDataAssignmentForUpdatingOfUserInterface);
  }
};
